(defvar *BF-MEM*)
(defvar *BF-POSITION*)
(defvar *BF-STREAM-POS*)
(defvar *BF-SIZE*)
(defvar *BF-LOOP-STACK*)

(defconstant +BF-MAXSIZE+ 30000)

(setf *BF-SIZE* 0)
(setf *BF-MEM* (make-array +BF-MAXSIZE+ :element-type 'byte))
(setf *BF-POSITION* 0)
(setf *BF-STREAM-POS* 0)
(setf *BF-LOOP-STACK* (make-array 5 :fill-pointer 0 :adjustable t)) ; This is our dynamic stack, it's adjustable and supports a fill-pointer.

(defun modify-mem (fn)
  (let ((v (elt *BF-MEM* *BF-POSITION*)))
    (setf (elt *BF-MEM* *BF-POSITION*) (funcall fn v))))

(defun ret-elem (&optional pos)
  (if pos
      (elt *BF-MEM* pos)
    (elt *BF-MEM* *BF-POSITION*)))

(defun vector-top (vec) (elt vec 0))

(defun bf-read (stream)
  (read-char stream))

(defun bf-set-stream-pos (c)
  (cond
    ((eql #\] c) (if (eql (ret-elem (vector-top *BF-LOOP-STACK*)) 0)
			(vector-pop *BF-LOOP-STACK*)
			(progn (setf *BF-STREAM-POS* (vector-top *BF-LOOP-STACK*))
			       (modify-mem #'1-))))
    (t (setf *BF-STREAM-POS* (1+ *BF-STREAM-POS*)))))
  
(defun bf-eval (c)
  (progn
    (cond ((eql #\> c) (setf *BF-POSITION* (1+ *BF-POSITION*)))
	  ((eql #\< c) (setf *BF-POSITION* (1- *BF-POSITION*)))
	  ((eql #\+ c) (modify-mem #'1+))
	  ((eql #\- c) (modify-mem #'1-))
	  ((eql #\. c) (princ (ret-elem)))
	  ((eql #\, c) (modify-mem #'(lambda (x) (setf x (read-char *standard-input*)))))
	  ((eql #\[ c) (vector-push *BF-POSITION* *BF-LOOP-STACK*))
	  ((eql #\] c) (if (eql (ret-elem (vector-top *BF-LOOP-STACK*)) 0)
			   (vector-pop *BF-LOOP-STACK*)
			   (setf *BF-POSITION* (vector-top *BF-LOOP-STACK*))))
	  (t (princ "Unrecognized Command")))))

; TODO: LOOP-STACK records position in memory to goto, not in the stream...

(defun string-end (arr n)
  (let ((var (eql (length arr) n)))
    (princ var)
    var))

(defun bf-repl ()
  ;; (do* ((line (read-line))
  ;; 	(c (char line *BF-STREAM-POS*))
  ;; 	   (if (string-end line *BF-STREAM-POS*) (setf c NIL) (setf c (char line *BF-STREAM-POS*))))
  ;;      ((equal (length line) *BF-STREAM-POS*))

  
       


  
  (loop (let ((line (read-line)))
	  (loop (let ((c (char line *BF-STREAM-POS*))) ;; <-- It's because we grab the new character before checking if we can....
		  (when (string-end line *BF-STREAM-POS*) (return))
		  (princ c)
		  (bf-eval c)
		  (bf-set-stream-pos c))))))

;;  (let ((c (read-char)))
;;    (unless (eq c #\newline)
;;      (bf-eval c)
;;      (bf-repl))))
